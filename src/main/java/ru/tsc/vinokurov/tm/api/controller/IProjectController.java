package ru.tsc.vinokurov.tm.api.controller;

public interface IProjectController {

    void showProjectList();

    void clearProjects();

    void createProject();

    void removeProjectByIndex();

    void removeProjectById();

    void showProjectByIndex();

    void showProjectById();

    void updateProjectByIndex();

    void updateProjectById();

    void changeStatusById();

    void changeStatusByIndex();

    void startProjectById();

    void startProjectByIndex();

    void completeProjectById();

    void completeProjectByIndex();

}
