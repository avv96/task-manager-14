package ru.tsc.vinokurov.tm.component;

import ru.tsc.vinokurov.tm.api.controller.ICommandController;
import ru.tsc.vinokurov.tm.api.controller.IProjectController;
import ru.tsc.vinokurov.tm.api.controller.IProjectTaskController;
import ru.tsc.vinokurov.tm.api.controller.ITaskController;
import ru.tsc.vinokurov.tm.api.repository.ICommandRepository;
import ru.tsc.vinokurov.tm.api.repository.IProjectRepository;
import ru.tsc.vinokurov.tm.api.repository.ITaskRepository;
import ru.tsc.vinokurov.tm.api.service.ICommandService;
import ru.tsc.vinokurov.tm.api.service.IProjectService;
import ru.tsc.vinokurov.tm.api.service.IProjectTaskService;
import ru.tsc.vinokurov.tm.api.service.ITaskService;
import ru.tsc.vinokurov.tm.constant.ArgumentConst;
import ru.tsc.vinokurov.tm.constant.TerminalConst;
import ru.tsc.vinokurov.tm.controller.CommandController;
import ru.tsc.vinokurov.tm.controller.ProjectController;
import ru.tsc.vinokurov.tm.controller.ProjectTaskController;
import ru.tsc.vinokurov.tm.controller.TaskController;
import ru.tsc.vinokurov.tm.enumerated.Status;
import ru.tsc.vinokurov.tm.model.Project;
import ru.tsc.vinokurov.tm.model.Task;
import ru.tsc.vinokurov.tm.repository.CommandRepository;
import ru.tsc.vinokurov.tm.repository.ProjectRepository;
import ru.tsc.vinokurov.tm.repository.TaskRepository;
import ru.tsc.vinokurov.tm.service.CommandService;
import ru.tsc.vinokurov.tm.service.ProjectService;
import ru.tsc.vinokurov.tm.service.ProjectTaskService;
import ru.tsc.vinokurov.tm.service.TaskService;
import ru.tsc.vinokurov.tm.util.DateUtil;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Date;

public final class Bootstrap {

    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final ICommandController commandController = new CommandController(commandService);

    private final ITaskRepository taskRepository = new TaskRepository();

    private final ITaskService taskService = new TaskService(taskRepository);

    private final ITaskController taskController = new TaskController(taskService);

    private final IProjectRepository projectRepository = new ProjectRepository();

    private final IProjectService projectService = new ProjectService(projectRepository);

    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    private final IProjectController projectController = new ProjectController(projectService, projectTaskService);

    private final IProjectTaskController projectTaskController = new ProjectTaskController(projectTaskService);

    public void close() {
        System.exit(0);
    }

    public void processCommand(final String command) {
        if (command == null || command.isEmpty()) return;
        switch (command) {
            case TerminalConst.COMMANDS:
                commandController.showCommands();
                break;
            case TerminalConst.ARGUMENTS:
                commandController.showArguments();
                break;
            case TerminalConst.HELP:
                commandController.showHelp();
                break;
            case TerminalConst.VERSION:
                commandController.showVersion();
                break;
            case TerminalConst.ABOUT:
                commandController.showAbout();
                break;
            case TerminalConst.INFO:
                commandController.showSystemInfo();
                break;
            case TerminalConst.EXIT:
                close();
                break;
            case TerminalConst.TASK_CREATE:
                taskController.createTask();
                break;
            case TerminalConst.TASK_CLEAR:
                taskController.clearTasks();
                break;
            case TerminalConst.TASK_LIST:
                taskController.showTaskList();
                break;
            case TerminalConst.PROJECT_CREATE:
                projectController.createProject();
                break;
            case TerminalConst.PROJECT_CLEAR:
                projectController.clearProjects();
                break;
            case TerminalConst.PROJECT_LIST:
                projectController.showProjectList();
                break;
            case TerminalConst.TASK_REMOVE_BY_ID:
                taskController.removeTaskById();
                break;
            case TerminalConst.TASK_REMOVE_BY_INDEX:
                taskController.removeTaskByIndex();
                break;
            case TerminalConst.TASK_UPDATE_BY_ID:
                taskController.updateTaskById();
                break;
            case TerminalConst.TASK_UPDATE_BY_INDEX:
                taskController.updateTaskByIndex();
                break;
            case TerminalConst.TASK_SHOW_BY_ID:
                taskController.showTaskById();
                break;
            case TerminalConst.TASK_SHOW_BY_INDEX:
                taskController.showTaskByIndex();
                break;
            case TerminalConst.PROJECT_REMOVE_BY_ID:
                projectController.removeProjectById();
                break;
            case TerminalConst.PROJECT_REMOVE_BY_INDEX:
                projectController.removeProjectByIndex();
                break;
            case TerminalConst.PROJECT_UPDATE_BY_ID:
                projectController.updateProjectById();
                break;
            case TerminalConst.PROJECT_UPDATE_BY_INDEX:
                projectController.updateProjectByIndex();
                break;
            case TerminalConst.PROJECT_SHOW_BY_ID:
                projectController.showProjectById();
                break;
            case TerminalConst.PROJECT_SHOW_BY_INDEX:
                projectController.showProjectByIndex();
                break;
            case TerminalConst.TASK_CHANGE_STATUS_BY_ID:
                taskController.changeStatusById();
                break;
            case TerminalConst.TASK_CHANGE_STATUS_BY_INDEX:
                taskController.changeStatusByIndex();
                break;
            case TerminalConst.TASK_START_BY_ID:
                taskController.startTaskById();
                break;
            case TerminalConst.TASK_START_BY_INDEX:
                taskController.startTaskByIndex();
                break;
            case TerminalConst.TASK_COMPLETE_BY_ID:
                taskController.completeTaskById();
                break;
            case TerminalConst.TASK_COMPLETE_BY_INDEX:
                taskController.completeTaskByIndex();
                break;
            case TerminalConst.PROJECT_CHANGE_STATUS_BY_ID:
                projectController.changeStatusById();
                break;
            case TerminalConst.PROJECT_CHANGE_STATUS_BY_INDEX:
                projectController.changeStatusByIndex();
                break;
            case TerminalConst.PROJECT_START_BY_ID:
                projectController.startProjectById();
                break;
            case TerminalConst.PROJECT_START_BY_INDEX:
                projectController.startProjectByIndex();
                break;
            case TerminalConst.PROJECT_COMPLETE_BY_ID:
                projectController.completeProjectById();
                break;
            case TerminalConst.PROJECT_COMPLETE_BY_INDEX:
                projectController.completeProjectByIndex();
                break;
            case TerminalConst.TASK_BIND_TO_PROJECT:
                projectTaskController.bindTaskToProject();
                break;
            case TerminalConst.TASK_UNBIND_FROM_PROJECT:
                projectTaskController.unbindTaskFromProject();
                break;
            case TerminalConst.TASK_SHOW_LIST_BY_PROJECT_ID:
                taskController.showTaskListByProjectId();
                break;
            default:
                commandController.showErrorCommand(command);
        }
    }

    public void processCommand() {
        try (final BufferedReader reader = new BufferedReader(new InputStreamReader(System.in))) {
            String command;
            System.out.print("Enter command: ");
            while ((command = reader.readLine()) != null) {
                processCommand(command);
                System.out.print("Enter command: ");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void processArgument(final String arg) {
        if (arg == null || arg.isEmpty()) return;
        switch (arg) {
            case ArgumentConst.COMMANDS:
                commandController.showCommands();
                break;
            case ArgumentConst.ARGUMENTS:
                commandController.showArguments();
                break;
            case ArgumentConst.HELP:
                commandController.showHelp();
                break;
            case ArgumentConst.VERSION:
                commandController.showVersion();
                break;
            case ArgumentConst.ABOUT:
                commandController.showAbout();
                break;
            case ArgumentConst.INFO:
                commandController.showSystemInfo();
                break;
            default:
                commandController.showErrorArgument(arg);
        }
    }

    public boolean processArgument(final String[] args) {
        if (args == null || args.length < 1) return false;
        for (String arg : args) {
            processArgument(arg);
        }
        return true;
    }

    public void run(final String[] args) {
        if (processArgument(args)) close();
        initDemoData();
        commandController.showWelcome();
        processCommand();
    }

    private void initData() {
        taskService.create("DEMO TASK 0", "DESC #0");
        taskService.create("DEMO TASK 1", "DESC #2");
        taskService.create("DEMO TASK 2", "DESC #3");
        projectService.create("DEMO Project 0", "DESC #0");
        projectService.create("DEMO Project 1", "DESC #1");
        projectService.create("DEMO Project 2", "DESC #2");
    }

    private void initDemoData() {
        projectService.add(new Project("DEMO PROJECT", Status.IN_PROGRESS, DateUtil.toDate("2020.10.27")));
        projectService.add(new Project("TEST PROJECT", Status.COMPLETED, DateUtil.toDate("2018.10.27")));
        projectService.add(new Project("BEST PROJECT", Status.NOT_STARTED, DateUtil.toDate("2019.10.27")));
        taskService.add(new Task("GAMMA TASK 0", Status.COMPLETED, DateUtil.toDate("2029.10.27")));
        taskService.add(new Task("ALPHA TASK 1", Status.IN_PROGRESS, DateUtil.toDate("2018.10.27")));
        taskService.add(new Task("BETTA TASK 2", Status.IN_PROGRESS, DateUtil.toDate("2021.10.27")));
    }

}
